﻿#ifndef FACADE_H
#define FACADE_H

#include "basefacade.h"
#include "module/searchbox.h"
#include "module/torus.h"
#include "module/bell.h"
#include "module/popup.h"

#include "module/note.h"
#include "module/note2.h"
#include "function/servant.h"

#include "m_fhs.h"
#include "head/g_bootfunction.h"
#include "head/g_function.h"
#include "support/pie.h"
#include "support/basereceiver.h"
#include "support/bookmarks.h"
#include "support/commandstore.h"
#include "support/simpleconfigurator.h"

#include <QFileSystemWatcher>
#include <QTimerEvent>

#ifdef Q_OS_WIN
#include "support/reg_winver.h"
#endif

#define MAG_URL "magnet:?xt=urn:btih:"
#define PAN_URL "http://pan.baidu.com/s/"

#define sk_gre 0x0f00
#define sk_blu 0x0f01   //0x0f01=3841
#define sk_pik 0x0f02
#define sk_def 0x0f03

#define skin_blu ui->body->setStyleSheet("image: url(:/res/Mario_Blue.png)")
#define skin_gre ui->body->setStyleSheet("image: url(:/res/Mario_Green.png)")
#define skin_pik ui->body->setStyleSheet("image: url(:/res/Mario_Pink.png)")

namespace Ui {
class Facade;
}

enum SP_Form
{
    Form_None,      //Ctrl+Alt+Backspace
    Form_MD5,       //J
    Form_SHA1,      //L
    Form_Web,       //W
    Form_Magnet,    //M
    Form_Pan,        //P
    Form_StrLen,
    Form_PrintScr
};

class Facade : public BaseFacade
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = 0);
    ~Facade();

    void keyPressEvent(QKeyEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    //    void mouseReleaseEvent(QMouseEvent *);
    void timerEvent(QTimerEvent *event);

    void startUp();
    void startReceiver();
    void endReceiver();
    void receivedMessage(const QByteArray &data);
    void parseCommand(const QString &cmd);
    void executeCommand(int code);

    //  void initializeDB();
    bool connectStorage();
    bool db_try();
    void initializeServant();
    //  bool openMenuDB(const QString &db_file);
    void aboutVersion();

    void setSkin();

    void showNote();
    void showNote2();
    void invokeCalc();
    void showSearchBox();
    //   void showThroughPanel();
    void showHardwareInfo();

    void ring(const QString &, const QString &);
    void pop(const QString &, const QString &);
    void say(const QString &, unsigned sec=4);

    void createMenu();
    void generateMenu();
    void reloadMenu();
    void reloadMenuAction();

    void createOptionMenu();
    void createSysToolMenu();
    void createPropsMenu();
    void createAppMenu();
    void createCommandMenu();
    void createLocationMenu();
    void createToolMenu();
    void reloadToolboxMenu();

    void createFavoriteMenu();
    void createFavoriteMenuItem();
    void reloadFavoriteMenu();

    void setStartup();
    void unsetStartup();

    void createToolboxMenu();
    void watchOptDirectory();

    void cmd_screenShot();

    //实用快捷键
    void util_reset();
    void util_generateUuid();
    void util_verifyMD5();
    void util_verifySHA1();
    void util_webThrough();
    void util_magnet();
    void util_baiduPan();
    void util_stringLength();
    void util_grubScreen();
    // void util_grubRect();

    void onTrayStateChanged();

signals:
    void sendRequest(QCryptographicHash::Algorithm method,const QString &fileName);

private slots:
    void processMenuAction(QAction *action);
    void processFavoriteAction(QAction *action);
    void processToolboxAction(QAction *action);
    void processsSkinAction(QAction *action);
    void receiveResult(QCryptographicHash::Algorithm method,const QByteArray &result);

private:
    Ui::Facade *ui;

    int dbTry_failure;
    QString shotLocation;

    SP_Form form;
    bool isWorking;
    CommandStore cmdStore;
    Bookmarks bookmarks;
    SimpleConfigurator config;

    Servant *worker;
    QThread servantThread;

    BaseReceiver *receiver;
    QThread recvThread;

    QSqlDatabase db;
    Popup *popup;
    Note *note;
    Note2 *note2;
    SearchBox *searchBox;

    int monitorId;
    int monitorInterval;

    int autoHideId;
    int autoHideInterval;

    QMenu *m_favorite;
    QMap<QString,QString> menuItems;
    QMap<QString,QString> toolboxMenuItems;
};

#endif // FACADE_H
