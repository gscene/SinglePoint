﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define PRE_SCRSHOTS "/Screenshots/"
#define e_note "/note.txt"
#define e_note21 "/note21.txt"
#define e_note22 "/note22.txt"

#define README "../share/readme.html"
#define Calc "calc"
#define Launcher "Launcher "        //注意有个空格

#define SCHED_ONCE "sched_once"
#define SCHED_PERIOD "sched_period"

#define TD_THROUGH "td_through"

#define T_LOCATION "location"
#define T_TOOL "tool"
#define T_COMMAND "command"
#define T_PROPS "props"
#define T_SYSTOOL "system"
#define T_APP "app"
#define T_URL "url"

#define MENU_GET_VARS "select label,detail from td_menu_preset where category='%1'"
#define THR_GET_VARS "select label,detail from td_through where category='%1'"

#endif // M_FHS_H
