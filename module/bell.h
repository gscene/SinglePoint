﻿#ifndef BELL_H
#define BELL_H

#include <QWidget>
#include <QApplication>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QString>
#include <QLabel>
#include <QPainter>
#include <QBitmap>
#include <QDesktopWidget>
#include <QTimer>
#include <QPoint>
#include <QRect>
#include <QFont>

#include "belltitle.h"

class Bell : public QWidget
{
    Q_OBJECT

public:
    explicit Bell(QWidget *parent = 0);

    void showMe();
    void setMsg(const QString &,const QString &);

    void myMove();
    void myStay();
    void myClose();
private:
    BellTitle *titleW;

    QLabel *content;
    QPoint normalPoint;
    QRect deskRect;

    QTimer *timerShow;//平滑显示的定时器
    QTimer *timerStay;//停留时间的定时器
    QTimer *timerClose;//关闭淡出的定时器

protected:
    void paintEvent(QPaintEvent *);
};

#endif // BELL_H
