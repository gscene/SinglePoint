﻿#ifndef NOTE2_H
#define NOTE2_H

#include <QDialog>
#include "m_fhs.h"
#include "support/sp_env.h"

namespace Ui {
class Note2;
}

class Note2 : public QDialog
{
    Q_OBJECT

public:
    explicit Note2(QWidget *parent = 0);
    ~Note2();

    void setUserLand(const QString &location);

    void closeEvent(QCloseEvent *);
    void save();
    void firstLoad();

private:
    Ui::Note2 *ui;
    QString userLand;
};

#endif // NOTE2_H
