﻿#include "bell.h"

Bell::Bell(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);

    setWindowFlags(
                Qt::FramelessWindowHint |
                Qt::WindowDoesNotAcceptFocus |
                Qt::WindowStaysOnTopHint |
                Qt::Tool
                );

    titleW=new BellTitle(this);
    content=new QLabel(this);

    content->setWordWrap(true);
    content->setAlignment(Qt::AlignTop);
    content->setFixedSize(300,150);

    QVBoxLayout*mainLayout=new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->addWidget(titleW);
    mainLayout->addWidget(content);
    content->setMargin(5);
    setLayout(mainLayout);

    setFixedSize(sizeHint().width(),sizeHint().height());

    timerShow=new QTimer(this);
    connect(timerShow,&QTimer::timeout,this,&Bell::myMove);

    timerStay=new QTimer(this);
    connect(timerStay,&QTimer::timeout,this,&Bell::myStay);

    timerClose=new QTimer(this);
    connect(timerClose,&QTimer::timeout,this,&Bell::myClose);
}

void Bell::setMsg(const QString &title,const QString &content)
{
    titleW->setTitleText(title);
    QFont font=this->font();
    font.setPointSize(14);
    this->content->setFont(font);
    this->content->setText(content);
}

void Bell::paintEvent(QPaintEvent *)
{
    QBitmap bitmap(this->size());
    bitmap.fill(Qt::white);
    QPainter painter(this);
    painter.setBrush(QBrush(QColor(250,240,230)));
    painter.setPen(QPen(QBrush(QColor(255,222,173)),4));
    painter.drawRoundedRect(bitmap.rect(),5,5);
    setMask(bitmap);
}

void Bell::showMe()
{
    QDesktopWidget *deskTop=QApplication::desktop();
    deskRect=deskTop->availableGeometry();
    normalPoint.setX(deskRect.width()-rect().width());
    normalPoint.setY(deskRect.height()-rect().height());
    move(normalPoint.x(),768-1);

    show();
    timerShow->start(4);        //控制上升速度
}

void Bell::myMove()
{
    static int beginY=QApplication::desktop()->height();
    beginY--;
    move(normalPoint.x(),beginY);
    if(beginY<=normalPoint.y())
    {
        timerShow->stop();
        timerStay->start(1000);
        beginY=QApplication::desktop()->height();
    }
}

void Bell::myClose()
{
    static double tran=1.0;
    tran-=0.1;

    if(tran<=0.0)
    {
        timerClose->stop();
        tran=1.0;
        this->close();
    }
    else
        setWindowOpacity(tran);
}

void Bell::myStay()
{
    static int timeCount=0;
    timeCount++;
    if(timeCount>= 5)           //控制停留时间
    {
        timerStay->stop();
        timerClose->start(200);
        timeCount=0;
    }
}
