﻿#include "torus.h"
#include "ui_torus.h"

Torus::Torus(QWidget *parent) :
    TransDialog(parent),
    ui(new Ui::Torus)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    TORUS_BLU;
}

Torus::~Torus()
{
    delete ui;
}

void Torus::mouseDoubleClickEvent(QMouseEvent *)
{
    this->hide();
}

void Torus::display(const QString &words, unsigned sec)
{
    ui->body->setText(words);
    this->show();
    QTimer::singleShot(1000 * sec,this,&Torus::hide);
}

void Torus::setStyle(int num)
{
    if(num == sk_blu)
        TORUS_BLU;
    else if(num == sk_pik)
        TORUS_PIK;
    else if(num == sk_gre)
        TORUS_GRE;
    else
        TORUS_BLU;
}
