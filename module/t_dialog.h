﻿#ifndef TDIALOG_H
#define TDIALOG_H

#include <QDialog>
#include <QMouseEvent>
#include <QPoint>

class TransDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TransDialog(QWidget *parent = 0);

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void detectVisible();

protected:
    QPoint dPos;
};

#endif // TDIALOG_H
