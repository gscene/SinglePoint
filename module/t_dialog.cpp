﻿#include "t_dialog.h"

TransDialog::TransDialog(QWidget *parent) :
    QDialog(parent)
{
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);
}

void TransDialog::mousePressEvent(QMouseEvent *event)
{
    //   winPos=this ->pos();
    //   mosPos= event ->globalPos();
    //   dPos=mosPos - winPos;
    dPos=event->globalPos() - pos();
}

void TransDialog::mouseMoveEvent(QMouseEvent *event)
{
    this->move(event ->globalPos() - dPos );
}

void TransDialog::detectVisible()
{
    if(this->isVisible())
        this->hide();
    else
        this->show();
}
