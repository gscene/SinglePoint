﻿#ifndef BASEFACADE_H
#define BASEFACADE_H

#include "common/t_facade.h"
#include "m_fhs.h"

class BaseFacade : public TransFacade
{
    Q_OBJECT

public:
    explicit BaseFacade(QWidget *parent = 0)
        : TransFacade(parent)
       {
           userLand = Location::document + APP_ROOT;
           configLocation=QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
           localLocation=QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
           dataLocation=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
           cacheLocation=QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
           genericConfigLocation=QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation);
           genericDataLocation=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);

           QDir dir(userLand);
           if(!dir.exists())
               dir.mkpath(userLand);

           if(getUid().isEmpty())
               putUid(QUuid::createUuid().toString());
       }

    virtual void startUp(){}
    virtual void setSkin(int){}
    virtual void createMenu(){}
    virtual void createOptionMenu(){}

    QString getUid() const
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        return reg.value("uid").toString();
    }
    void putUid(const QString &uid)\
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        reg.setValue("uid",uid);
    }

    virtual int getSkin() const
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        return reg.value("skin").toInt();
    }
    virtual void putSkin(int skin)
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        reg.setValue("skin",skin);
    }

protected:
    QString userLand;

    QString configLocation;
    QString localLocation;      //同config
    QString dataLocation;
    QString cacheLocation;

    QString genericConfigLocation;
    QString genericDataLocation;
};

#endif // BASEFACADE_H
