﻿#ifndef SERVANT_H
#define SERVANT_H

#include <QObject>
#include <QByteArray>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QCryptographicHash>
#include <QThread>

#define BASE_SIZE (48 * 1024 * 1024)
#define MAX_SIZE (512 * 1024 * 1024)

class Servant : public QObject
{
    Q_OBJECT

public:
    explicit Servant(QObject *parent = 0);

    static QByteArray verify(QCryptographicHash::Algorithm method,const QString &fileName);

signals:
    void sendResult(QCryptographicHash::Algorithm method,const QByteArray &result);

private slots:
    void verifyHash(QCryptographicHash::Algorithm method,const QString &fileName);
};

#endif // SERVANT_H
