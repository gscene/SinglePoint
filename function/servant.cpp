﻿#include "servant.h"

Servant::Servant(QObject *parent) : QObject(parent)
{}

void Servant::verifyHash(QCryptographicHash::Algorithm method, const QString &fileName)
{
    QByteArray data=Servant::verify(method,fileName);
    emit sendResult(method,data);
}

QByteArray Servant::verify(QCryptographicHash::Algorithm method, const QString &fileName)
{
    QByteArray data("NULL");
    QFile file(fileName);
    QFileInfo fileInfo(fileName);
    if(fileInfo.size() > MAX_SIZE)
    {
        if(file.open(QFile::ReadOnly))
        {
            QCryptographicHash hash(method);
            if(hash.addData(&file))
                data=hash.result().toHex();
        }
    }
    else
    {
        if(file.open(QFile::ReadOnly))
            data=QCryptographicHash::hash(file.readAll(),method).toHex();
    }

    return data;
}
